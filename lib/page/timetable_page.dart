import 'package:flutter/material.dart';

class Timetable extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
    body: buildWidget,
    //drawer: NavigationDrawerWidget(),
    appBar: AppBar(
      title: Text('ตารางเรียน'),
      centerTitle: true,
      backgroundColor: Colors.green.shade600,
    ),
  );
}

var buildWidget = ListView(
  children: <Widget>[
    Column(
      children: <Widget>[
        Container(
          width: double.infinity,
          height: 200,
          child: Image.network(
            "https://cdn.discordapp.com/attachments/881538016856920094/1069009523592941568/image.png",
            fit: BoxFit.cover,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
            ],
          ),
        ),
        Divider(color: Colors.grey),
        subject1(),
        subject2(),
        subject3(),
        subject4(),
        subject5(),
      ],
    )
  ],
);




Widget subject1() {
  return ListTile(
    leading: Icon(Icons.perm_identity),
    title: Text("Web Programming (88624359-59)"),
    subtitle: Text("อาจารย์วรวิทย์ วีระพันธุ์"),
  );
}

Widget subject2() {
  return ListTile(
    leading: Icon(Icons.perm_identity),
    title: Text("Object-Oriented Analysis and Design (88624459-59)"),
    subtitle: Text("อาจารย์วรวิทย์ วีระพันธุ์"),
  );
}

Widget subject3() {
  return ListTile(
    leading: Icon(Icons.perm_identity),
    title: Text("Multimedia Programming for Multiplatforms (88634259-59)"),
    subtitle: Text("ผู้ช่วยศาสตราจารย์ ดร.อุรีรัฐ สุขสวัสดิ์ชน"),
  );
}

Widget subject4() {
  return ListTile(
    leading: Icon(Icons.perm_identity),
    title: Text("Software Testing (88624559-59)"),
    subtitle: Text("อาจารย์ ดร.พิเชษ วะยะลุน"),
  );
}

Widget subject5() {
  return ListTile(
    leading: Icon(Icons.perm_identity),
    title: Text("Mobile Application Development I (88634459-59)"),
    subtitle: Text("ผู้ช่วยศาสตราจารย์ ดร.จักริน สุขสวัสดิ์ชน"),
  );
}