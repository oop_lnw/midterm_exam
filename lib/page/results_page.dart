import 'package:flutter/material.dart';

class Results extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
    body: buildBody,
    appBar: AppBar(
      title: Text('ผลการเรียน'),
      centerTitle: true,
      backgroundColor: Colors.blue.shade700,
    ),
  );
}

var buildBody = ListView(
  children: <Widget>[
    Column(
      children: <Widget>[
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "ภาคการศึกษาที่ 1/2565",
                  style: TextStyle(fontSize: 30),
                ),
              )
            ],
          ),
        ),
        Container(
          width: double.infinity,
          height: 250,
          child: Image.network(
            "https://cdn.discordapp.com/attachments/992408015582023730/1069717115558437024/image.png",
            fit: BoxFit.cover,
          ),
        ),
        Divider(color: Colors.grey),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "ภาคการศึกษาที่ 2/2565",
                  style: TextStyle(fontSize: 30),
                ),
              )
            ],
          ),
        ),
        Container(
          width: double.infinity,
          height: 250,
          child: Image.network(
            "https://cdn.discordapp.com/attachments/992408015582023730/1069717520166162564/image.png",
            fit: BoxFit.cover,
          ),
        ),
        Divider(color: Colors.grey),
      ],
    )
  ],
);