import 'package:flutter/material.dart';

class Notifications extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
    body: buildBody,
    appBar: AppBar(
      title: Text('การแจ้งเตือน'),
      centerTitle: true,
      backgroundColor: Colors.red.shade700,
    ),
  );
}

var buildBody = ListView(
  children: <Widget>[
    Column(
      children: <Widget>[
        notifications1(),
        notifications2(),
        notifications3(),
        notifications4(),
        notifications5(),
        notifications6(),
        notifications7(),
        notifications8(),
        notifications9(),
        notifications10(),
        // subject(),
      ],
    )
  ],
);

Widget notifications1() {
  return ListTile(
    leading: Icon(Icons.notifications_active_outlined),
    title: Text("28 Dec 2022 at 15:00-16:00 Moblie Application Development I at room: IF-3C01"),
    subtitle: Text("28 Dec 2022 14:45"),
  );
}

Widget notifications2() {
  return ListTile(
    leading: Icon(Icons.notifications_off_outlined),
    title: Text("22 Dec 2022 at 10:00-11:50 Moblie Application Development I at room: IF-4C01"),
    subtitle: Text("22 Dec 2022 09:46"),
  );
}

Widget notifications3() {
  return ListTile(
    leading: Icon(Icons.notifications_outlined),
    title: Text("17 Dec 2022 at 17:00-18:50 Web Programming at room: IF-3C01"),
    subtitle: Text("17 Dec 2022 16:46"),
  );
}

Widget notifications4() {
  return ListTile(
    leading: Icon(Icons.notifications_off_outlined),
    title: Text("9 Dec 2022 at 13:00-15:50 Introduction to Natural Language Processing at room: IF-6T02"),
    subtitle: Text("9 Dec 2022 12:50"),
  );
}

Widget notifications5() {
  return ListTile(
    leading: Icon(Icons.notifications_off_outlined),
    title: Text("9 Dec 2022 at 10:00-11:50 Moblie Application Development I at room: IF-4C01"),
    subtitle: Text("9 Dec 2022 09:46"),
  );
}

Widget notifications6() {
  return ListTile(
    leading: Icon(Icons.notifications_off_outlined),
    title: Text("8 Dec 2022 at 15:00-16:50 Software Testing at room: IF-3C03"),
    subtitle: Text("8 Dec 2022 14:46"),
  );
}

Widget notifications7() {
  return ListTile(
    leading: Icon(Icons.notifications_outlined),
    title: Text("8 Dec 2022 at 13:00-14:50 Object-Oriented Analysis and Design at room: IF-3C01"),
    subtitle: Text("8 Dec 2022 12:53"),
  );
}

Widget notifications8() {
  return ListTile(
    leading: Icon(Icons.notifications_off_outlined),
    title: Text("7 Dec 2022 at 10:00-11:50 Multimedia Programming for Multiplatforms at room: IF-4C01"),
    subtitle: Text("7 Dec 2022 09:45"),
  );
}

Widget notifications9() {
  return ListTile(
    leading: Icon(Icons.notifications_outlined),
    title: Text("5 Dec 2022 at 17:00-18:50 Web Programming at room: IF-3M210"),
    subtitle: Text("5 Dec 2022 16:46"),
  );
}

Widget notifications10() {
  return ListTile(
    leading: Icon(Icons.notifications_off_outlined),
    title: Text("1 Dec 2022 at 13:00-14:50 Object-Oriented Analysis and Design at room: IF-3M210"),
    subtitle: Text("1 Dec 2022 12:48"),
  );
}

