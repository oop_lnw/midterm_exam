import 'package:flutter/material.dart';

class Personal extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
    body: buildBodyWidget,
    appBar: AppBar(
      title: Text('63160071 Nutthaphol Intaranok'),
      centerTitle: true,
      backgroundColor: Colors.amber,
    ),
  );
}

Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("09090989"),
    subtitle: Text("TON"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.amber,
      onPressed: () {},
    ),
  );
}

Widget mailListTitle() {
  return ListTile(
    leading: Icon(Icons.mail),
    title: Text("63160071@go.buu.ac.th"),
  );
}

Widget locationListTitle() {
  return ListTile(
    leading: Icon(Icons.home) ,
    title: Text("Sriracha Chonburi 20110"),
    subtitle: Text("HOME"),
    trailing: IconButton(
      icon: Icon(Icons.location_on_outlined),
      color: Colors.amber,
      onPressed: () {},
    ),
  );
}

Widget bloodtype() {
  return ListTile(
    leading: Icon(Icons.bloodtype_outlined),
    title: Text("A"),
  );
}

Widget birthday() {
  return ListTile(
    leading: Icon(Icons.cake),
    title: Text("18 MAY 2001"),
  );
}
Widget faculties() {
  return ListTile(
    leading: Icon(Icons.school_outlined),
    title: Text("Faculty of Informatice"),
    subtitle: Text("Computer Science (CS)"),
  );
}
var buildBodyWidget = ListView(
  children: <Widget>[
    Column(
      children: <Widget>[
        Container(
          width: double.infinity,
          height: 250,
          child: Image.network(
            "https://cdn.discordapp.com/attachments/992408015582023730/1069707612540633138/IMG_5076.jpg",
            fit: BoxFit.cover,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
            ],
          ),
        ),
        Divider(color: Colors.grey),
        mobilePhoneListTile(),
        mailListTitle(),
        faculties(),
        locationListTitle(),
        birthday(),
        bloodtype(),
      ],
    )
  ],
);

