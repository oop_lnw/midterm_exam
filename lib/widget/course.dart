import 'package:flutter/material.dart' show Color;

class Course {
  final String title, description, iconSrc;
  final Color color;

  Course({
    required this.title,
    this.description = 'ดูรายละเอียดเพิ่มเติม',
    this.iconSrc = "assets/icons/ios.svg",
    this.color = const Color(0xFFE0C404),
  });
}

final List<Course> courses = [
  Course(
    title: "หลักสูตรใหม่",
  ),
  Course(
    title: "ตารางสอบกลางภาค",
    color: const Color(0xFF03B49B),
  ),
];

final List<Course> recentCourses = [
  Course(title: "ร่วมบริจาคเลือด"),
  Course(
    title: "ยื่นคำร้องเพิ่ม/ลดรายวิชา",
    color: const Color(0xFF03B49B),
  ),
  Course(title: "ทำบัตรนิสิตรูปแบบใหม่"),
];